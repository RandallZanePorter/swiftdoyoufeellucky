//
//  ThirdViewController.swift
//  PorterGuesserApp
//  This is my own work. Except for the tab icons. I stole those from the assignment pdf.
//  Created by Randall Zane Porter on 2/27/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class StatisticsViewController: UIViewController {

    @IBOutlet weak var minLBL: UILabel!
    @IBOutlet weak var maxLBL: UILabel!
    @IBOutlet weak var meanLBL: UILabel!
    @IBOutlet weak var stdDevLBL: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let min:Int? = attemptsLog.min()
        minLBL.text = String(min == nil ? 0 : min!)
        let max:Int? = attemptsLog.max()
        maxLBL.text = String(max == nil ? 0 : max!)
        var sum:Int = 0
        for att in attemptsLog {
            sum += att
        }
        var mean = Double(sum) / Double(attemptsLog.count)
        if mean.isNaN { mean = 0.0 }
        meanLBL.text = String(round(mean * 10) / 10)
        
        var stdDev:Double = 0.0
        for att in attemptsLog {
            stdDev += (Double(att) - mean) * (Double(att) - mean)
        }
        stdDev /= Double(attemptsLog.count)
        if stdDev.isNaN { stdDev = 0.0 }
        stdDevLBL.text = String(round(stdDev.squareRoot() * 10) / 10)
        
    }
    @IBAction func clear(_ sender: Any) {
        minLBL.text = "0"
        maxLBL.text = "0"
        meanLBL.text = "0"
        stdDevLBL.text = "0"
        answersLog.removeAll()
        attemptsLog.removeAll()
    }
    
}
