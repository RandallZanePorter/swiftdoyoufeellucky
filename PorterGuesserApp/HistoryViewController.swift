//
//  SecondViewController.swift
//  PorterGuesserApp
//  This is my own work. Except for the tab icons. I stole those from the assignment pdf.
//  Created by Randall Zane Porter on 2/27/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

var answersLog:[Int] = []
var attemptsLog:[Int] = []

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var historyTV: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answersLog.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "trial")!
        //cell.textLabel?.text = trials[indexPath.row]
        cell.textLabel?.text = "Correct Answer: \(answersLog[indexPath.row])"
        cell.detailTextLabel?.text = "# Attempts: \(attemptsLog[indexPath.row])"
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        historyTV?.reloadData()
    }


}

