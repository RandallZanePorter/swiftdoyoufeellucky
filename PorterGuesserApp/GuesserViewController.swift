//
//  FirstViewController.swift
//  PorterGuesserApp
//  This is my own work. Except for the tab icons. I stole those from the assignment pdf.
//  And I got the displayMessage() function from the assignment pdf.
//  Created by Randall Zane Porter on 2/27/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class GuesserViewController: UIViewController {

    var num:Int!
    var attempts:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBOutlet weak var guessTF: UITextField!
    @IBOutlet weak var hotColdLBL: UILabel!
    @IBOutlet weak var checkOutlet: UIButton!
    @IBAction func checkButton(_ sender: Any) {
        if num == nil {
            hotColdLBL.text = "There is no number yet."
            return
        } else {
            let guess:Int! = Int(guessTF.text!)
            attempts += 1
            if guess != nil {
                if guess == num {
                    hotColdLBL.text = "Correct!"
                    answersLog.append(num)
                    attemptsLog.append(attempts)
                    checkOutlet.isEnabled = false
                    displayMessage()
                } else {
                    hotColdLBL.text = "Too \(guess > num ? "High" : "Low")"
                }
            } else {
                hotColdLBL.text = "That wasn't an integer."
            }
        }
    }
    @IBAction func generateButton(_ sender: Any) {
        num = Int.random(in: 1...10)
        hotColdLBL.text = "..."
        attempts = 0
        checkOutlet.isEnabled = true
    }
    
    func displayMessage(){
        let alert = UIAlertController(title: "Well done", message: "You got it in \(attempts) tries", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}

